const index = require("../index"); // Import index to run index.rl on this file

function sphereVolume(r) {
  const formula = (4 / 3) * Math.PI * r * r * r;
  return formula;
}

function isEmptyOrSpaces(str) {
  return str === null || str.match(/^ *$/) !== null;
}

// Function for calculate sphere
function input() {
  console.log("+==============================================+");
  console.log("+                   Sphere Volume                +");
  console.log("+==============================================+");
  index.rl.question(`Enter the radius of the Tube: `, (jari) => {
    if (!isNaN(jari)) {
      if (isEmptyOrSpaces(jari)) {
        console.log("Value must not have space!");
      }
      const value = sphereVolume(jari);
      console.log("Sphere volume is : ", value);
      index.menu2();
    } else {
      console.log(`Diameter and Height must be a number\n`);
      input();
    }
  });
}

module.exports = { input };

// V = 4/3 × π × r³
