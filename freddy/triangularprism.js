// Import readline
const index = require("../index"); // Import index to run rl on this file

// Function to calculate triangular prism volume
function prism(length, width, height) {
  return length * width * height * 0.5;
}
/* Way 1 */
// Function for inputing length of triangular prism
function inputLength() {
  index.rl.question(`Length: `, (length) => {
    if (!isNaN(length)) {
      inputWidth(length);
    } else {
      console.log(`Length must be a number\n`);
      inputLength();
    }
  });
}

// Function for inputing width of triangular prism
function inputWidth(length) {
  index.rl.question(`Width: `, (width) => {
    if (!isNaN(width)) {
      inputHeight(length, width);
    } else {
      console.log(`Width must be a number\n`);
      inputWidth(length);
    }
  });
}

// Function for inputing height of beam
function inputHeight(length, width) {
  index.rl.question(`Height: `, (height) => {
    if (!isNaN(height)) {
      console.log(`\nprism: ${prism(length, width, height)}\n`);
      index.menu2();
    } else {
      console.log(`Height must be a number\n`);
      inputHeight(length, width);
    }
  });
}
/* End Way 1 */

module.exports = { inputLength }; // Export inputLength and input function, so another file can call it
