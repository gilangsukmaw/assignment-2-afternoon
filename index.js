// Import readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const prism = require("./freddy/triangularprism"); // function mas freddy
const sphere = require("./Gilang/Sphere"); // function mas Gilang
const tube = require("./Yhido/tubeYhido"); // funtion mas Yhido

function isEmptyOrSpaces(str) {
  return str === null || str.match(/^ *$/) !== null;
}

// Function to display the menu
function menu() {
  console.log(`       Menu`);
  console.log(`===================`);
  console.log(`1. Prism`);
  console.log(`2. Sphere`);
  console.log(`3. Tube`);
  console.log(`4. Exit`);
  rl.question(`Choose option: `, (option) => {
    if (!isNaN(option)) {
      // If option is a number it will go here
      if (option == 1) {
        prism.inputLength();
        //function mas freddy
      } else if (option == 2) {
        sphere.input();

        // function mas gilang
      } else if (option == 3) {
        // function mas Yhido
        tube.inputRadius();
      } else if (option == 4) {
        rl.close(); // It will close the program
      } else if (isEmptyOrSpaces(option)) {
        console.log("Option must not have a space");
        menu(); // If option is not 1 to 3, it will go back to the menu again
      } else {
        console.log(`Option must be 1 to 3!\n`);
        menu(); // If option is not 1 to 3, it will go back to the menu again
      }
    } else {
      // If option is not a number it will go here
      console.log(`Option must be number!\n`);
      menu(); // If option is not 1 to 3, it will go back to the menu again
    }
  });
}
function menu2() {
  console.log(`Mau Menghitung lagi ?  `);
  rl.question("Y/N ? ", (option2) => {
    if (option2 == "y" || option2 == "Y") {
      menu();
    } else if (option2 == "n" || option2 == "N") {
      rl.close();
    } else {
      console.log("Must Choose Y or N");
      menu2();
    }
  });
}

menu(); // call the menu function to display the menu

module.exports.rl = rl; // export rl to make another can run the readline
module.exports.isEmptyOrSpaces = isEmptyOrSpaces;
module.exports.menu2 = menu2;
