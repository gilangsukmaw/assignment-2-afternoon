/* If we use callback function, we can't add another logic outside the function */

// Import readline
const index = require("../index");
const isEmptyOrSpaces = require("../index");
function Tube(radius, length) {
  return Math.PI * radius ** 2 * length;
}

function inputRadius() {
  index.rl.question(`Radius of Tube: `, (radius) => {
    if (!isNaN(radius) && radius > 0) {
      inputLength(radius);
    } else if (radius < 0) {
      console.log(`Radius must be a positive number`);
      inputRadius();
    } else if (isEmptyOrSpaces(radius)) {
      console.log("Cannot be Empty");
    } else {
      console.log(`Radius must be a number \n`);
      inputRadius();
    }
  });
}

function inputLength(radius) {
  index.rl.question(`Length of Tube: `, (length) => {
    if (!isNaN(length) && length > 0) {
      console.log(`Volume of Tube : ${Tube(radius, length)} meter`);
      index.menu2();
    } else if (length < 0) {
      console.log(`Length must be a positive number`);
      inputLength(radius, length);
    } else if (isEmptyOrSpaces(radius)) {
      console.log("Cannot be Empty");
    } else {
      console.log(`Length must be a number`);
      inputLength(radius, length);
    }
  });
}

console.log(`WELCOME !`);
console.log(`Tube Calculator (Meter)! `);

module.exports = { inputRadius };
